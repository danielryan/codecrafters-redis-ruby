require "socket"

class YourRedisServer
  def initialize(port)
    @port = port
  end

  def start
    # You can use print statements as follows for debugging, they'll be visible when running tests.
    # puts("Logs from your program will appear here!")

    # Uncomment this block to pass the first stage
    server = TCPServer.new(@port)
    # client = server.accept
    loop do
      Thread.start(server.accept) do |client|
        while data = client.recv(1024) do
          command = parse(data)
          next if command.empty?
          handle(client, command)
        end
        client.close
      end
    end

  end

  def parse(data)
    data = data.split("\r\n")
    data.shift
    data.each_slice(2).map { |_, command| command }
  end

  def handle(client, command)
    case command[0].upcase
    when "PING"
      client.write("+PONG\r\n")
    when "ECHO"
      client.write("$#{command[1].size}\r\n#{command[1]}\r\n")
    else
      client.write("-ERR unknown command '#{command[0]}'\r\n")
    end
  end
end

YourRedisServer.new(6379).start
